#!/bin/bash

# Définir les variables
vnetName="MonVNetYiming"
vnetAddressPrefix="10.0.12.0/24"
subnetBaseName="SubnetYiming"
subnetAddressPrefix="10.0.12.0/28"
subnetCount=16
resourceGroupName="OCC_ASD_Yiming"


# Créer le réseau virtuel
az network vnet create --resource-group $resourceGroupName \
                       --name $vnetName \
                       --address-prefixes $vnetAddressPrefix

# Créer le sous-réseaux "Subnet Admin"
subnetName="SubnetAdminYiming"
    subnetAddressPrefix="10.0.12.0/28" # Chaque sous-réseau a une plage différente
    az network vnet subnet create --resource-group $resourceGroupName \
                                   --vnet-name $vnetName \
                                   --name $subnetName \
                                   --address-prefixes $subnetAddressPrefix
# Créer les autre sous-réseaux 1~15
for i in {1..15}
do
    subnetName="$subnetBaseName$i"
    subnetAddressPrefix="10.0.12.$((i*16))/28" # Chaque sous-réseau a une plage différente
    az network vnet subnet create --resource-group $resourceGroupName \
                                   --vnet-name $vnetName \
                                   --name $subnetName \
                                   --address-prefixes $subnetAddressPrefix
done
